# Android client for the Zprava Project

## Note about error related to missing `.iml` file (Cannot load project or modules).

Delete all the `.iml` files in `zprava-android-activities`. On Linux/OSX:

```sh
find zprava-android-acvitities -iname '*.iml*' -delete
```

Or on Windows (untested):

```powershell
get-childitem -Path zprava-android-activities -Include *.iml -recurse | Remove-Item -Confirm
```
