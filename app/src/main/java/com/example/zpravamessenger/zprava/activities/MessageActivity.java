package com.example.zpravamessenger.zprava.activities;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import com.example.zpravamessenger.zprava.ApiCaller;
import com.example.zpravamessenger.zprava.Message;
import com.example.zpravamessenger.zprava.MessageRecyclerViewAdapter;
import com.example.zpravamessenger.zprava.R;
import com.example.zpravamessenger.zprava.MessageRecyclerViewAdapter;

import java.util.ArrayList;

public class MessageActivity extends AppCompatActivity implements  SwipeRefreshLayout.OnRefreshListener{
    private ArrayList<Message> mMessages;
    private int conversationID;
    private MessageRecyclerViewAdapter adapter;
    private ApiCaller apiCaller;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("notificationType");  //get the type of message from MyGcmListenerService 1 - lock or 0 -Unlock
            if (type.equals("msg")) // 1 == lock
            {
                if(Integer.parseInt( intent.getStringExtra("conversationID")) == conversationID){
                    onRefresh();
                }else Toast.makeText(getApplication(), "New Message from " + intent.getStringExtra("sender") +" in conversation " + intent.getStringExtra("conversationName"), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplication(), "There has been a change to your conversations.", Toast.LENGTH_LONG).show() ;
            }
        }
    };
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        Intent intent = getIntent();
        this.conversationID = intent.getIntExtra("conversationId", -1);

        this.mDrawerLayout = findViewById(R.id.MessageParentLayout);
        TextView headerTextView = findViewById(R.id.message_header_textview);
        headerTextView.setText(intent.getStringExtra("conversationName"));
        this.apiCaller = new ApiCaller(this);
        recyclerView = findViewById(R.id.messageRecycler);
        mSwipeRefreshLayout = findViewById(R.id.message_swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                onRefresh();
            }
        });
        LocalBroadcastManager.getInstance(MessageActivity.this).registerReceiver(
                broadcastReceiver, new IntentFilter("com.example.zpravamessaenger.zprava.action.MESSAGE_RECEIVED"));

    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        apiCaller.getMessages(this, this.conversationID);
    }
    
    public void loadRecyclerViewData(ArrayList<Message> messages) {
        mMessages = messages;
        adapter = new MessageRecyclerViewAdapter(this, mMessages);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.scrollToPosition(messages.size() - 1);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void messageSendFunction(View view){

        String type = "text/plain";
        AutoCompleteTextView MessageTextView = findViewById(R.id.messageTextView);
        String messageText = MessageTextView.getText().toString();
        if(!messageText.trim().equals("")) {
            mSwipeRefreshLayout.setRefreshing(true);
            String encodedString = Base64.encodeToString(messageText.getBytes(), Base64.NO_WRAP);
            apiCaller.sendMessage(this, this.conversationID, type, encodedString);
        }
    }

    public void messageSuccess(Message message) {
        if (adapter == null) {
            ArrayList<Message> temp = new ArrayList<Message>();
            temp.add(message);
            loadRecyclerViewData(temp);
        } else {
            AutoCompleteTextView messageTextView = findViewById(R.id.messageTextView);
            messageTextView.setText("");
            mMessages.add(message);
            adapter.notifyDataSetChanged();
            recyclerView.scrollToPosition(mMessages.size() - 1);
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void returnToConversationList(View view) {
        Intent intent = new Intent(getApplicationContext(), ConversationListActivity.class);
        startActivity(intent);
    }

    public void messageFailure() {
        Toast toast = Toast.makeText(this, "Failed to Send Message", Toast.LENGTH_LONG);
        toast.show();
        mSwipeRefreshLayout.setRefreshing(false);
    }
    
    public void onError() {
        Toast toast = Toast.makeText(this, "Failed to Refresh Messages", Toast.LENGTH_LONG);
        toast.show();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        LocalBroadcastManager.getInstance(MessageActivity.this).registerReceiver(broadcastReceiver, new IntentFilter("com.example.zpravamessaenger.zprava.action.MESSAGE_RECEIVED"));
    }

    @Override
    protected void onPause() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    public void openMessageNavBar(View view) {
        this.mDrawerLayout.openDrawer(Gravity.START);
    }
}
