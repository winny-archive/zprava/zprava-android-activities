package com.example.zpravamessenger.zprava;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.zpravamessenger.zprava.activities.MessageActivity;

import java.util.ArrayList;

public class ConversationRecyclerViewAdapter extends RecyclerView.Adapter<ConversationRecyclerViewAdapter.ViewHolder>{
    private ArrayList<Conversation> mConversations = new ArrayList<>();
    private Context mContext;

    public ConversationRecyclerViewAdapter(Context mContext, ArrayList<Conversation> mConversations) {
        this.mConversations = mConversations;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.conversation_listitem, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        return mConversations.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Conversation conversation = mConversations.get(i);
        viewHolder.conversationText.setText(conversation.getName());
        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MessageActivity.class);
                intent.putExtra("conversationName", conversation.getName());
                intent.putExtra("conversationId", conversation.getId());
                mContext.startActivity(intent);
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView conversationText;
        ImageView conversationImage;
        ConstraintLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.parentLayout = itemView.findViewById(R.id.conversationListItemParent);
            this.conversationText = itemView.findViewById(R.id.conversationTextView);
            this.conversationImage = itemView.findViewById(R.id.conversationImageView);
        }
    }
}