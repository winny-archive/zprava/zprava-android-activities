package com.example.zpravamessenger.zprava.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.util.Log;

import com.example.zpravamessenger.zprava.ApiCaller;
import com.example.zpravamessenger.zprava.R;
import com.example.zpravamessenger.zprava.SaveSharedPreference;
import com.example.zpravamessenger.zprava.User;
import com.example.zpravamessenger.zprava.activities.ConversationListActivity;
import com.example.zpravamessenger.zprava.activities.SignupActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    // class vars
    private ApiCaller apiCaller;
    private String username;
    private String password;
    private String deviceID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.apiCaller = new ApiCaller(this);
    }

    // helper method to ensure proper data entry
    private boolean validEntry(String username, String password, String deviceID) {
        return (!username.equals("") && !password.equals("")&& deviceID!=null);
    }

    public void login(View view) {
        EditText usernameEditText = findViewById(R.id.usernameEditText);
        EditText passwordEditText = findViewById(R.id.passwordEditText);

        username = usernameEditText.getText().toString();
        password = passwordEditText.getText().toString();
        deviceID = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + deviceID);

        if (validEntry(username, password, deviceID)) {
            Map<String, String> userData = new HashMap<>();
            userData.put("username", username);
            userData.put("password", password);
            userData.put("device_id", deviceID);
            Log.d(TAG, "Refreshed token 2: " + deviceID);
            apiCaller.userLogin(this, userData);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "Invalid Entry...", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void setAuthToken(String token) {
        Log.d(TAG, "Refreshed token: " + FirebaseInstanceId.getInstance().getInstanceId());
        SaveSharedPreference.setUserName(this, username);
        SaveSharedPreference.setAuthToken(this, token);
        apiCaller.getUserInfo(this);
    }

    public void setUserId(User user) {
        SaveSharedPreference.setUserId(this, String.valueOf(user.getUserId()));
        Intent intent = new Intent(getApplicationContext(), ConversationListActivity.class);
        startActivity(intent);
    }

    public void signup(View view) {
        Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
        startActivity(intent);
    }

    public void onError(String error) {
        Toast toast = Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG);
        toast.show();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }
}
