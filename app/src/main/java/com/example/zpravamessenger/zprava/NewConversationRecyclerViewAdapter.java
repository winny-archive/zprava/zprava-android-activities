package com.example.zpravamessenger.zprava;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class NewConversationRecyclerViewAdapter extends RecyclerView.Adapter<NewConversationRecyclerViewAdapter.ViewHolder>{
    private ArrayList<String> mUsernames = new ArrayList<>();
    private Context mContext;

    public NewConversationRecyclerViewAdapter(Context mContext, ArrayList<String> mUsernames) {
        this.mUsernames = mUsernames;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.new_conversation_listitem, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        return mUsernames.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final String username = mUsernames.get(i);
        viewHolder.usernameText.setText(username);
        viewHolder.deleteUserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO Figure out how to remove this item from the list when this button is clicked
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView usernameText;
        ImageView deleteUserImage;
        ConstraintLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.parentLayout = itemView.findViewById(R.id.newConoItemParentLayout);
            this.usernameText = itemView.findViewById(R.id.newConvoItemText);
            this.deleteUserImage = itemView.findViewById(R.id.newConvoDeleteItemButton);
        }
    }
}
