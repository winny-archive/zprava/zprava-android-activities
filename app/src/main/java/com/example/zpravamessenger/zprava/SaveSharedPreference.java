package com.example.zpravamessenger.zprava;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SaveSharedPreference {
    static String PREF_USER_NAME = "username";
    static String PREF_AUTH_TOKEN = "authtoken";
    static String PREF_USER_ID = "userid";

    static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setUserName(Context context, String userName) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static String getUserName(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_NAME, "");
    }

    // Sets the user's token for all client side POST/PUT
    public static void setAuthToken(Context context, String authToken) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_AUTH_TOKEN, authToken);
        editor.commit();
    }

    public static String getAuthToken(Context context) {
        return getSharedPreferences(context).getString(PREF_AUTH_TOKEN, "");
    }

    // Sets the user's token for all client side POST/PUT
    public static void setUserId(Context context, String userId) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_ID, userId);
        editor.commit();
    }

    public static String getUserId(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_ID, "");
    }

    public static void clearSharedPreferences(Context context) {
        getSharedPreferences(context).edit().clear().commit();
    }

}
