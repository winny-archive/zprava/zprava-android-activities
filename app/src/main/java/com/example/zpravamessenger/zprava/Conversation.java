package com.example.zpravamessenger.zprava;

import java.util.ArrayList;

public class Conversation {
    private ArrayList<User> users;
    private String name;
    private int id;
    private String last_active;
    private String created;
    private String updated;
    private String deleted;

    public Conversation(ArrayList<User> users, String name, String last_active, int id, String created, String updated, String deleted) {
        this.users = users;
        this.name = name;
        this.last_active = last_active;
        this.id = id;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }
}
