package com.example.zpravamessenger.zprava.activities;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;


import com.example.zpravamessenger.zprava.ApiCaller;
import com.example.zpravamessenger.zprava.Conversation;
import com.example.zpravamessenger.zprava.ConversationRecyclerViewAdapter;
import com.example.zpravamessenger.zprava.R;
import com.example.zpravamessenger.zprava.SaveSharedPreference;

import java.util.ArrayList;

public class ConversationListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView recyclerView;
    private ConversationRecyclerViewAdapter adapter;
    private ApiCaller apiCaller;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String type = intent.getStringExtra("notificationType");  //get the type of message from MyGcmListenerService 1 - lock or 0 -Unlock
            if (type.equals("msg")) // 1 == lock
            {
                Toast.makeText(getApplication(), "New Message from " + intent.getStringExtra("sender") +" in conversation " + intent.getStringExtra("conversationName"), Toast.LENGTH_LONG).show();
            } else {
                onRefresh();
            }
        }
    };
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //If no user is logged in then redirect to the login activity
        if (SaveSharedPreference.getUserName(this).length() == 0) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        } else {
            setContentView(R.layout.activity_conversation_list);
            setTitle(SaveSharedPreference.getUserName(this) + "'s Chat list");

            final ConversationListActivity currentActivity = this;
            final NavigationView navigationView = findViewById(R.id.conversation_list_navigation);
            View headerView = navigationView.getHeaderView(0);
            TextView navbarHeadingText = headerView.findViewById(R.id.conversation_list_navbar_heading);
            navigationView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(MenuItem menuItem) {
                            menuItem.setChecked(true);
                            mDrawerLayout.closeDrawers();

                            if (String.valueOf(menuItem.getTitle()).equals("Logout")) {
                                apiCaller.logOut(currentActivity);
                            }
                            return true;
                        }
                    });

            navbarHeadingText.setText(SaveSharedPreference.getUserName(this));
            this.mDrawerLayout = findViewById(R.id.conversationListParentLayout);
            this.apiCaller = new ApiCaller(this);
            recyclerView = findViewById(R.id.conversationRecycler);
            mSwipeRefreshLayout = findViewById(R.id.swipe_container);
            mSwipeRefreshLayout.setOnRefreshListener(this);
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    onRefresh();
                }
            });
            LocalBroadcastManager.getInstance(ConversationListActivity.this).registerReceiver(
                    broadcastReceiver, new IntentFilter("com.example.zpravamessaenger.zprava.action.MESSAGE_RECEIVED"));
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        apiCaller.getConversations(this);
    }

    public void loadRecyclerViewData(ArrayList<Conversation> conversations) {
        adapter = new ConversationRecyclerViewAdapter(this, conversations);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void logOutFunction(View view) {
        apiCaller.logOut(this);
    }

    public void sendToNewConversationFunction(View view) {
        Intent intent = new Intent(this, NewConversationActivity.class);
        startActivity(intent);
    }

    public void onError() {
        Toast toast = Toast.makeText(this, "Failed To Refresh Conversation List", Toast.LENGTH_LONG);
        toast.show();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void logoutSuccess() {
        SaveSharedPreference.clearSharedPreferences(this);
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }

    public void logoutFailure() {
        Toast toast = Toast.makeText(this, "Failed To Log Out", Toast.LENGTH_LONG);
        toast.show();
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        //registerReceiver(statusReceiver,mIntent);
        LocalBroadcastManager.getInstance(ConversationListActivity.this).registerReceiver(broadcastReceiver, new IntentFilter("com.example.zpravamessaenger.zprava.action.MESSAGE_RECEIVED"));
    }

    @Override
    protected void onPause() {

        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    public void openConvoListNavBar(View view) {
        this.mDrawerLayout.openDrawer(Gravity.START);
    }
}
